import random
import string
import hashlib

"""def generer_lignes(nombre_lignes):
    for i in range(0, nombre_lignes):
        print("voici un nombre aleatoire: "+ str(random.randint(0, 1000)))

generer_lignes(100)"""

def gen_lettre():
    return random.choice(string.ascii_letters)

def gen_int():
    return random.randint(0, 9)

def gen_chiffre_lettre():
    hasard = random.randint(1,2)
    if hasard == 1:
        return (gen_lettre()+str(gen_int()))
    else:
        return str(gen_int()) + gen_lettre()

def gen_nonce(x):
    nonce = ""
    for i in range(x):
        hasard = random.randint(0, 1)
        if hasard ==0:
            nonce = nonce + str(gen_int())
        else:
            nonce = nonce + gen_lettre()
    return nonce

"""def x_0(difficulte, message)->bool:
    for i in range(difficulte):
        if message[i] != '0'
            return False
        return True"""

def proof_of_work(difficulte: int, transactions: str, last_hash : str):
    hash =""
    while not hash.startswith(difficulte*'0'):
        block = transactions+last_hash+gen_nonce(64)
        hash = hashlib.sha256(block.encode('utf-8')).hexdigest()
    return hash

print(proof_of_work(16, "blazazazdazddazdazaasxas", "tyfiojhhoih" ))















